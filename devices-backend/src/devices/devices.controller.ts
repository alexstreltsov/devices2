import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { DevicesService } from './devices.service';
import { CreateDeviceDto } from './dtos/create-device.dto';
import { UpdateDeviceDto } from './dtos/update-device.dto';
import { Device } from './schemas/device.schema';

@Controller('devices')
export class DevicesController {
  constructor(private readonly deviceService: DevicesService) {}

  @Get()
  async getAll(): Promise<Device[]> {
    return this.deviceService.getAll();
  }

  @Get(':id')
  async getById(@Param(':id') id: string): Promise<Device> {
    return this.deviceService.getById(id);
  }

  @Post()
  async create(@Body() createDeviceDto: CreateDeviceDto) {
    await this.deviceService.create(createDeviceDto);
  }

  @Put(':id')
  async update(
    @Param(':id') id: string,
    @Body() updateDeviceDto: UpdateDeviceDto,
  ): Promise<Device> {
    return this.deviceService.update(id, updateDeviceDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<Device> {
    return this.deviceService.remove(id);
  }
}
