export class UpdateDeviceDto {
    readonly id: string
    readonly name: string
    readonly description: string
}