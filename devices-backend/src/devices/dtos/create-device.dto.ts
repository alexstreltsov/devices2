export class CreateDeviceDto {
    readonly id: string
    readonly name: string
    readonly description: string
}