import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DevicesModule } from './devices/devices.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    DevicesModule,
    MongooseModule.forRoot(
      process.env.DB_CONNECTION_STRING,
    ),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
