**Backend for devices project**

👉  Get started with the following commands:

$ cd devices-backend

$ npm run start

**Frontend for devices project**

👉  Get started with the following commands:

$ cd frontend

$ ng serve --open