import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeviceListComponent } from './device/device-list/device-list.component';
import { DeviceLayoutComponent } from './device/device-layout/device-layout.component';
import { DeviceFormComponent } from './device/device-form/device-form.component';
import { DeviceNotFoundComponent } from './device/device-not-found/device-not-found.component';

const routes: Routes = [
  {
    path: 'device',
    component: DeviceLayoutComponent,
    children: [
      { path: 'list', component: DeviceListComponent },
      { path: 'add', component: DeviceFormComponent },
      { path: 'not_found/:id', component: DeviceNotFoundComponent },
      { path: ':id', component: DeviceFormComponent },
    ]
  },
  {
    path: '**',
    redirectTo: 'device/list'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
