import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeviceFormComponent } from './device-form/device-form.component';
import { DeviceListComponent } from './device-list/device-list.component';
import { DeviceNotFoundComponent } from './device-not-found/device-not-found.component';

const routes: Routes = [
  { path: 'list', component: DeviceListComponent },
  { path: 'add', component: DeviceFormComponent },
  { path: 'not_found/:id', component: DeviceNotFoundComponent },
  { path: ':id', component: DeviceFormComponent },
  { path: '**', redirectTo: 'list' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeviceRoutingModule { }
