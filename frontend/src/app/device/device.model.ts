export type DeviceModel = {
  id?: number,
  name: string,
  description: string,
}