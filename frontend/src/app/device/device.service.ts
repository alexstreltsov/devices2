import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { DeviceModel } from './device.model';

const DEVICE_LIST: DeviceModel[] = [
  { id:1, name: 'DirectorPC', description: 'Description 1' },
  { id:2, name: 'Another PC', description: 'Description 2' },
  { id:3, name: 'Super PC', description: 'Super - puper!11' },
]

@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  updateEvent = new BehaviorSubject<DeviceModel[]>([])

  constructor() {
    this.getList()
  }

  getList() {
    this.updateEvent.next(DEVICE_LIST)
  }

  get(id: number) {
    return DEVICE_LIST.find(device => device.id === id)
  }

  create(newDevice: DeviceModel) {
    const newId = DEVICE_LIST.length ? DEVICE_LIST[DEVICE_LIST.length-1].id! + 1 : 1
    newDevice.id = newId
    DEVICE_LIST.push(newDevice)
    this.getList()
  }

  update(changedDevice: DeviceModel) {
    const existedDevice = DEVICE_LIST.find(device => device.id === changedDevice.id)
    if (existedDevice) {
      existedDevice.name = changedDevice.name
      existedDevice.description = changedDevice.description

      this.getList()
    }
  }

  delete(deviceIdToDelete: number) {
    const deviceIndexToDelete = DEVICE_LIST.findIndex(device => device.id === deviceIdToDelete)
    if (deviceIndexToDelete > -1) {
      DEVICE_LIST.splice(deviceIndexToDelete, 1)

      this.getList()
    }
  }
}
