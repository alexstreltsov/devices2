import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-device-not-found',
  templateUrl: './device-not-found.component.html',
  styleUrls: ['./device-not-found.component.less']
})
export class DeviceNotFoundComponent implements OnInit {

  deviceId: string

  constructor(
    private readonly _route: ActivatedRoute,
  ) {
    this.deviceId = this._route.snapshot.params['id']
  }

  ngOnInit(): void {
  }

}
