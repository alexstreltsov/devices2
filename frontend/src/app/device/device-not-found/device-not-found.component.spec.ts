import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceNotFoundComponent } from './device-not-found.component';

describe('DeviceNotFoundComponent', () => {
  let component: DeviceNotFoundComponent;
  let fixture: ComponentFixture<DeviceNotFoundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeviceNotFoundComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
