import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeviceRoutingModule } from './device-routing.module';
import { DeviceLayoutComponent } from './device-layout/device-layout.component';
import { DeviceListComponent } from './device-list/device-list.component';
import { RouterModule } from '@angular/router';
import { ClarityModule } from '@clr/angular';
import { DeviceFormComponent } from './device-form/device-form.component';
import { FormsModule } from '@angular/forms';
import { DeviceNotFoundComponent } from './device-not-found/device-not-found.component';


@NgModule({
  declarations: [
    DeviceLayoutComponent,
    DeviceListComponent,
    DeviceFormComponent,
    DeviceNotFoundComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    DeviceRoutingModule,
    RouterModule,
    ClarityModule,
  ]
})
export class DeviceModule { }
