import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SubscriptionLike } from 'rxjs';
import { DeviceModel } from '../device.model';
import { DeviceService } from '../device.service';


@Component({
  selector: 'app-device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['./device-list.component.less']
})
export class DeviceListComponent implements OnInit {

  isLoading = true
  deviceList: DeviceModel[] = []
  private _deviceListSubsription: SubscriptionLike

  constructor(
    @Inject(DeviceService) private readonly _deviceService: DeviceService,
    private _router: Router,
  ) {
    this._deviceListSubsription = this._deviceService.updateEvent.subscribe(deviceList => {
      this.deviceList = deviceList
      this.isLoading = false
    })
  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    this._deviceListSubsription.unsubscribe()
  }

  openDeviceFormHandler(deviceId: number | undefined) {
    if (deviceId) this._router.navigateByUrl('device/' + deviceId.toString())
  }

}