import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DeviceModel } from '../device.model';
import { DeviceService } from '../device.service';

@Component({
  selector: 'app-device-form',
  templateUrl: './device-form.component.html',
  styleUrls: ['./device-form.component.less']
})
export class DeviceFormComponent implements OnInit {

  isLoading = true
  isCreateMode = true
  title = "Добавить устройство"

  device: DeviceModel = {
    name: '',
    description: '',
  }

  constructor(
    @Inject(DeviceService) private readonly _deviceService: DeviceService,
    private readonly _route: ActivatedRoute,
    private readonly _router: Router,
  ) { }

  ngOnInit(): void {
    const deviceId = this._route.snapshot.params['id']
    if (deviceId) {
      this.isCreateMode = false
      const loadedDevice = this._deviceService.get(parseInt(deviceId, 10))
      if (loadedDevice) {
        this.device = loadedDevice
        this.isLoading = false
      }
      else this._router.navigateByUrl('device/not_found/' + deviceId)
    }
    else {
      this.isCreateMode = true
      this.isLoading = false
    }
  }

  createHandler() {
    this._deviceService.create(this.device);
    this.returnToList()
  }

  updateHandler() {
    if (this.device.id) {
      this._deviceService.update(this.device)
      this.returnToList()
    }
  }

  deleteHandler() {
    if (this.device.id) {
      if(confirm('Удалить устройство?')) {
        this._deviceService.delete(this.device.id)
        this.returnToList()
      }
    }
  }

  returnToList() {
    this._router.navigateByUrl('device/list')
  }
}
